package com.aws.dvac01.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("statuses")
public class DvaC01Controller {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<String> getStatus()
    {
        return Arrays.asList("UP");
    }
}
