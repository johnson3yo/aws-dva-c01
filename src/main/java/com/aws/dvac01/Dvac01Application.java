package com.aws.dvac01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Dvac01Application {

	public static void main(String[] args) {
		SpringApplication.run(Dvac01Application.class, args);
	}

}
